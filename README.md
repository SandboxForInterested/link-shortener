# Getting Started

## GOAL

Разработать REST-сервис, для укорачивания ссылок. У сервиса должно быть два метода для укорачивания ссылок и разворачивания коротких ссылок (аналог https://bitly.com/).

### Требования:
* При вызове метода POST или GET метода укорачивания возвращается короткая ссылка в ответ
* При вызове метода POST или GET метода разворачивания, возвращается длинная ссылка
* Удалять или обновлять уже добавленные ссылки нельзя
* Добавлять ссылки можно без авторизации
* Возможность локального запуска для тестирования

### Используемые технологии:
* Spring Boot
* Любая in memory коллекция для хранения ссылок
* Сборщик проектов (maven, gradle)
* Любые другие технологии на усмотрение разработчика

В readme.md файле к проекту должно быть указано как запустить сервис и какие аргументы нужно передавать в методы. Как плюс unit тесты и интеграционные тесты с использование testcontainers.


## Build project

For build project use maven command from base directory
```
  .\mvnw package
```

## Run project

For build project use command from base directory
```
  java -jar .\target\LinkShorter-0.0.1-SNAPSHOT.jar
```

### Usage

#### Create short link

Send POST request to endpoint **http://<server_address>: <port> /{shortLink}**

with body
```
{
    "baseLink":<base_link>
}
```
#### Get base link

Send GET request to endpoint **http://<server_address>: <port> /{shortLink}**

returned value looks like simple string
```
<base_link>
```

### Reference Documentation

For further reference, please consider the following sections:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.7.4/maven-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/2.7.4/maven-plugin/reference/html/#build-image)
* [Spring Configuration Processor](https://docs.spring.io/spring-boot/docs/2.7.4/reference/htmlsingle/#appendix.configuration-metadata.annotation-processor)
* [Spring Web](https://docs.spring.io/spring-boot/docs/2.7.4/reference/htmlsingle/#web)
* [Spring Boot Actuator](https://docs.spring.io/spring-boot/docs/2.7.4/reference/htmlsingle/#actuator)
* [Spring Boot DevTools](https://docs.spring.io/spring-boot/docs/2.7.4/reference/htmlsingle/#using.devtools)
* [Testcontainers](https://www.testcontainers.org/)

### Guides

The following guides illustrate how to use some features concretely:

* [Building a RESTful Web Service](https://spring.io/guides/gs/rest-service/)
* [Serving Web Content with Spring MVC](https://spring.io/guides/gs/serving-web-content/)
* [Building REST services with Spring](https://spring.io/guides/tutorials/rest/)
* [Building a RESTful Web Service with Spring Boot Actuator](https://spring.io/guides/gs/actuator-service/)

