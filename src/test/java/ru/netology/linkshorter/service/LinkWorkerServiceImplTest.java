package ru.netology.linkshorter.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;

import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import ru.netology.linkshorter.entity.LinkContainer;
import ru.netology.linkshorter.entity.LinkDto;
import ru.netology.linkshorter.repository.impl.LinkRepositoryInMemoryImpl;
import ru.netology.linkshorter.service.impl.LinkWorkerServiceImpl;

@SpringBootTest
class LinkWorkerServiceImplTest {

    static final String BASE_LINK = "BaseLink";

    static final String SHORT_LINK = "ShortLink";

    LinkContainer defaultLinkContainer = new LinkContainer(BASE_LINK,SHORT_LINK,1L);

    @MockBean
    LinkRepositoryInMemoryImpl repository;

    @Autowired
    LinkWorkerServiceImpl service;

    @Test
    void validateShortLink() {
    }

    @Test
    void createUniqueShortLink() {
        given(repository.saveNewShortLink(any())).willReturn(defaultLinkContainer);

        LinkDto linkDto = new LinkDto(BASE_LINK);

        Optional<LinkContainer> result = service.createShortLink(SHORT_LINK,linkDto);


        Mockito.verify(repository,Mockito.times(1))
            .getLinkByShortLink(anyString());
        Mockito.verify(repository,Mockito.times(1))
            .saveNewShortLink(any());

        assertThat(result.isPresent()).isTrue();
        assertThat(result.get()).isEqualTo(defaultLinkContainer);
    }

    @Test
    void createNotNonUniqueShortLink() {
        given(repository.getLinkByShortLink(any())).willReturn(defaultLinkContainer);

        LinkDto linkDto = new LinkDto(BASE_LINK);

        Optional<LinkContainer> result = service.createShortLink(SHORT_LINK,linkDto);

        Mockito.verify(repository,Mockito.times(0))
            .saveNewShortLink(any());
        Mockito.verify(repository,Mockito.times(1))
            .getLinkByShortLink(any());

        assertThat(result.isPresent()).isFalse();
    }

    @Test
    void getByRealShortLink() {
        given(repository.getLinkByShortLink(anyString())).willReturn(defaultLinkContainer);
        ArgumentCaptor<String> arg = ArgumentCaptor.forClass(String.class);

        Optional<String> baseLink = service.getBaseLink(SHORT_LINK);

        Mockito.verify(repository,Mockito.times(1))
            .getLinkByShortLink(arg.capture());

        assertThat(baseLink.isPresent()).isTrue();
        assertThat(baseLink.get()).isEqualTo(BASE_LINK);
    }

    @Test
    void getByUnknownShortLink() {
        given(repository.getLinkByShortLink(anyString())).willReturn(null);

        Optional<String> baseLink = service.getBaseLink(SHORT_LINK);

        Mockito.verify(repository,Mockito.times(1))
            .getLinkByShortLink(any());

        assertThat(baseLink.isPresent()).isFalse();
    }
}