package ru.netology.linkshorter.controller;


import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

import java.util.Optional;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper;
import ru.netology.linkshorter.entity.LinkContainer;
import ru.netology.linkshorter.entity.LinkDto;
import ru.netology.linkshorter.service.impl.LinkWorkerServiceImpl;

@SpringBootTest
@ContextConfiguration
class LinkShorterControllerTest {

    static final String BASE_LINK = "BaseLink";

    static final String SHORT_LINK = "ShortLink";

    static LinkContainer defaultLinkContainer = new LinkContainer(BASE_LINK, SHORT_LINK, 1L);
    static LinkDto defaultLinkDto = new LinkDto(BASE_LINK);
    static String json;


    @Autowired
    private WebApplicationContext context;

    @MockBean
    private LinkWorkerServiceImpl service;

    private MockMvc mvc;

    @BeforeAll
    static void init() throws Exception {
        json = new ObjectMapper().writeValueAsString(defaultLinkDto);
    }

    @BeforeEach
    void initBeforeEachTest() {
        mvc = MockMvcBuilders
            .webAppContextSetup(context)
            .build();
    }

    @Test
    void createNewLink() throws Exception {

        given(service.validateShortLink(anyString())).willReturn(true);
        given(service.createShortLink(anyString(), any())).willReturn(Optional.of(defaultLinkContainer));
        ArgumentCaptor<String> arg = ArgumentCaptor.forClass(String.class);

        mvc.perform(post("/" + SHORT_LINK).contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .content(json))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().string(BASE_LINK));

        Mockito.verify(service, Mockito.times(1))
            .validateShortLink(arg.capture());
        Mockito.verify(service, Mockito.times(1))
            .createShortLink(anyString(), any());

        assertThat(arg.getValue()).isEqualTo(SHORT_LINK);
    }

    @Test
    void createAlreadyCreatedLink() throws Exception {
        given(service.validateShortLink(anyString())).willReturn(true);
        given(service.getBaseLink(anyString())).willReturn(Optional.of(BASE_LINK));
        given(service.createShortLink(anyString(), any())).willReturn(Optional.empty());
        ArgumentCaptor<String> arg = ArgumentCaptor.forClass(String.class);

        mvc.perform(post("/" + SHORT_LINK).contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .content(json))
            .andDo(print())
            .andExpect(status().isNotModified())
            .andExpect(content().string("Short link already mapped"));

        Mockito.verify(service, Mockito.times(1))
            .validateShortLink(arg.capture());
        Mockito.verify(service, Mockito.times(1))
            .createShortLink(anyString(), any());

        assertThat(arg.getValue()).isEqualTo(SHORT_LINK);
    }

    @Test
    void createInvalidateShortLink() throws Exception {
        given(service.validateShortLink(anyString())).willReturn(false);
        ArgumentCaptor<String> arg = ArgumentCaptor.forClass(String.class);

        mvc.perform(post("/" + SHORT_LINK).contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .content(json))
            .andDo(print())
            .andExpect(status().isBadRequest())
            .andExpect(content().string("Incorrect character in url, required only -, 0-9, a-z"));

        Mockito.verify(service, Mockito.times(1))
            .validateShortLink(arg.capture());
        Mockito.verify(service, Mockito.times(0))
            .createShortLink(anyString(), any());

        assertThat(arg.getValue()).isEqualTo(SHORT_LINK);
    }

    @Test
    void getExistingLink() throws Exception {
        given(service.getBaseLink(anyString())).willReturn(Optional.of(BASE_LINK));
        ArgumentCaptor<String> arg = ArgumentCaptor.forClass(String.class);

        mvc.perform(get("/" + SHORT_LINK))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().string(BASE_LINK));

        Mockito.verify(service, Mockito.times(1))
            .getBaseLink(arg.capture());

        assertThat(arg.getValue()).isEqualTo(SHORT_LINK);
    }

    @Test
    void getNonExistingLink() throws Exception {
        given(service.getBaseLink(anyString())).willReturn(Optional.empty());
        ArgumentCaptor<String> arg = ArgumentCaptor.forClass(String.class);

        mvc.perform(get("/" + SHORT_LINK))
            .andDo(print())
            .andExpect(status().isNotFound());

        Mockito.verify(service, Mockito.times(1))
            .getBaseLink(arg.capture());

        assertThat(arg.getValue()).isEqualTo(SHORT_LINK);
    }
}