package ru.netology.linkshorter.repository;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ru.netology.linkshorter.entity.LinkContainer;
import ru.netology.linkshorter.repository.impl.LinkRepositoryInMemoryImpl;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class LinkRepositoryInMemoryImplTest {

    static final String BASE_LINK = "BaseLink";

    static final String SHORT_LINK = "ShortLink";

    LinkContainer defaultLinkContainer = new LinkContainer(BASE_LINK,SHORT_LINK,1L);

    @Autowired
    LinkRepositoryInMemoryImpl repository;

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void saveCorrectNewShortLink() {
        LinkContainer savedContainer = repository.saveNewShortLink(defaultLinkContainer);
        assertThat(savedContainer).isEqualTo(defaultLinkContainer);
    }

    @Test
    void getLinkByShortLink() {
        repository.saveNewShortLink(defaultLinkContainer);
        LinkContainer selectedContainer = repository.getLinkByShortLink(SHORT_LINK);
        assertThat(selectedContainer).isEqualTo(defaultLinkContainer);
    }
}