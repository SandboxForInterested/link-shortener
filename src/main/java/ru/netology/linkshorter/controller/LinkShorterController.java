package ru.netology.linkshorter.controller;


import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.netology.linkshorter.entity.LinkContainer;
import ru.netology.linkshorter.entity.LinkDto;
import ru.netology.linkshorter.service.LinkWorkerService;

@Slf4j
@Controller
@RequiredArgsConstructor
@RequestMapping("/")
public class LinkShorterController {

    private final LinkWorkerService service;

    @PostMapping(value = "{shortLink}")
    public ResponseEntity<String> createLink(@PathVariable String shortLink, @RequestBody LinkDto dto) {

        if (!service.validateShortLink(shortLink)) {
            return new ResponseEntity<>(
                "Incorrect character in url, required only -, 0-9, a-z",
                HttpStatus.BAD_REQUEST);
        }

        Optional<LinkContainer> container = service.createShortLink(shortLink, dto);

        return container.map(linkContainer -> ResponseEntity.ok(linkContainer.baseLink()))
            .orElseGet(() -> new ResponseEntity<>(
                "Short link already mapped",
                HttpStatus.NOT_MODIFIED));

    }

    @GetMapping(value = "{shortLink}")
    public ResponseEntity<String> getBaseLink(@PathVariable String shortLink) {
        return ResponseEntity.of(service.getBaseLink(shortLink));
    }

}
