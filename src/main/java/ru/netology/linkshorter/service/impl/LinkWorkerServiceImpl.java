package ru.netology.linkshorter.service.impl;

import java.util.Objects;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.netology.linkshorter.entity.LinkContainer;
import ru.netology.linkshorter.entity.LinkDto;
import ru.netology.linkshorter.repository.LinkRepository;
import ru.netology.linkshorter.service.LinkWorkerService;

@Slf4j
@Service
@RequiredArgsConstructor
public class LinkWorkerServiceImpl implements LinkWorkerService {

    private final String URL_REGEXP = "[-1-9a-zA-Z]+";

    private final LinkRepository repository;

    @Override
    public boolean validateShortLink(String shortLink) {
        Pattern pattern = Pattern.compile(URL_REGEXP);
        Matcher matcher = pattern.matcher(shortLink);
        return matcher.matches();
    }

    @Override
    public Optional<LinkContainer> createShortLink(String shortLink, LinkDto baseLink) {
        if(Objects.isNull(repository.getLinkByShortLink(shortLink))){
            LinkContainer container = new LinkContainer(
                baseLink.getBaseLink(),
                shortLink,
                System.currentTimeMillis());
            return Optional.of(repository.saveNewShortLink(container));
        }
        return Optional.empty();

    }

    @Override
    public Optional<String> getBaseLink(String shortLink) {
        return Optional.ofNullable(repository.getLinkByShortLink(shortLink))
            .map(LinkContainer::baseLink);
    }
}
