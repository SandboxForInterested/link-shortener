package ru.netology.linkshorter.service;

import java.util.Optional;
import ru.netology.linkshorter.entity.LinkContainer;
import ru.netology.linkshorter.entity.LinkDto;

public interface LinkWorkerService {

    boolean validateShortLink(String shortLink);

    Optional<LinkContainer> createShortLink(String shortLink, LinkDto baseLink);

    Optional<String> getBaseLink(String shortLink);

}
