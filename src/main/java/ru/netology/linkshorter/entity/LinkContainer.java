package ru.netology.linkshorter.entity;


public record LinkContainer(String baseLink, String shortLink, long createdAt) {

}
