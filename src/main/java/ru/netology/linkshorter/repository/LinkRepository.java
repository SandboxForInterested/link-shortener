package ru.netology.linkshorter.repository;

import ru.netology.linkshorter.entity.LinkContainer;

public interface LinkRepository {

    LinkContainer saveNewShortLink(LinkContainer container);

    LinkContainer getLinkByShortLink(String shortLink);

}
