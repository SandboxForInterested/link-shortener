package ru.netology.linkshorter.repository.impl;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.springframework.stereotype.Component;
import ru.netology.linkshorter.entity.LinkContainer;
import ru.netology.linkshorter.repository.LinkRepository;

@Component
public class LinkRepositoryInMemoryImpl implements LinkRepository {

    private final Map<String, LinkContainer> cache = new ConcurrentHashMap<>();

    @Override
    public LinkContainer saveNewShortLink(LinkContainer container) {
        return cache.put(container.shortLink(), container);
    }

    @Override
    public LinkContainer getLinkByShortLink(String shortLink) {
        return cache.getOrDefault(shortLink, null);
    }
}
